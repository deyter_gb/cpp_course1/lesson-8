/**
 * Created by AardWolf in 2021 year.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum PLAYER_FIGURES: char {
    X = 'X',
    O = 'O'
};

enum GAME_STATES {
    IN_PROGRESS,
    DRAW,
    PLAYER_WON,
    COMPUTER_WON
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

    struct Coord {
        uint xCoord;
        uint yCoord;
    };

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_startBtn_clicked();
    void onFieldBtn_clicked();

private:
    void AddButtonsToCellsArray();
    void ConnectBtnSlots();
    void setSymbolToCell(QPushButton* button, bool is_computer_step);
    void clearBgColor(uint xCoord, uint yCoord);
    void setBgColor(uint xCoord, uint yCoord, Qt::GlobalColor color);
    uint getFieldIndex(const uint &x, const uint &y);
    bool isWin();
    void computerMove();
    void MakeComputerMove(uint x, uint y);
    void setStatusText();
    QPushButton*** cells;
    size_t current_step;
    Coord human_last_coord;
    Coord computer_last_coord;
    Ui::MainWindow *ui;
    GAME_STATES game_state;

};
#endif // MAINWINDOW_H
