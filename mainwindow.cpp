/**
 * Created by AardWolf in 2021 year.
 */
#include <random>
#include <stdlib.h>
#include <chrono>
#include <QDebug>
#include <QMessageBox>
#include <QVector>
#include "mainwindow.h"
#include "ui_mainwindow.h"

// @TODO Implement flexible size of field.
// @TODO Refactor to get better structure of program.
// @TODO Create buttons programmatically if flexible size of field.
// @TODO Check ifWin function... again.
const uint FIELD_SIZE = 3;
const uint STREAK_TO_WIN = 3;

const size_t RANDOM_RANGE = 10000;
const size_t HALF_RANDOM_RANGE = 5000;

const char* X_COORD_PROPERTY = "xCoord";
const char* Y_COORD_PROPERTY = "yCoord";

const Qt::GlobalColor HUMAN_COLOR = Qt::GlobalColor::green;
const Qt::GlobalColor COMPUTER_COLOR = Qt::GlobalColor::yellow;

PLAYER_FIGURES HUMAN_FIGURE;
PLAYER_FIGURES COMPUTER_FIGURE;

const uint DRAW_TEXTS_LENGTH = 3;
std::string DRAW_TEXTS[] = {
    "<p align=\"center\">It's draw.</p>",
    "<p align=\"center\">Nobody wins.</p>",
    "<p align=\"center\">Game ended with draw.</p>"
};

const uint PLAYER_WINS_TEXTS_LENGTH = 4;
std::string PLAYER_WINS_TEXTS[] = {
    "<p align=\"center\">You won.</p>",
    "<p align=\"center\">Computer lose.</p>",
    "<p align=\"center\">You play better.</p>",
    "<p align=\"center\">It's lucky day.</p>"
};

const uint PLAYER_LOSE_TEXTS_LENGTH = 5;
std::string PLAYER_LOSE_TEXTS[] = {
    "<p align=\"center\">You lose.</p>",
    "<p align=\"center\">Computer won.</p>",
    "<p align=\"center\">You play worse.</p>",
    "<p align=\"center\">It's not a lucky day.</p>",
    "<p align=\"center\">You are loser.</p>"
};

int32_t getRandomNum(int32_t min, int32_t max)
{
    const static auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    static std::mt19937_64 generator(seed);
    std::uniform_int_distribution<int32_t> dis(min, max);
    return dis(generator);
};

///////////////////////////////////////////////// Some game logic.
/**
 * @brief MainWindow::isWin
 * In each cycle of checking we drop win strike to zero(not zero in the fact) for each new line which we check.
 * Then we goes through line and count win streak. If it's enough then player won.
 * If we found empty cell then we need to drop win streak to zero.
 * @return True if player won.
 */
bool MainWindow::isWin() {
    uint win_streak;
    // Check verticals.
    qDebug() << "Check verticals";
    for (int y = 0; y < FIELD_SIZE; y++) {
        win_streak = 1;
        for (int x = 1; x < FIELD_SIZE; x++) {
            qDebug() << "x:" << x << " y:" << y;
            qDebug() << "[x,y]:" << this->cells[y][x]->text() << "[x-1,y]" << this->cells[y][x - 1]->text();
            if (this->cells[y][x]->text().isEmpty() || this->cells[y][x]->text() != this->cells[y][x - 1]->text()) {
                win_streak = 1;
            }
            else if (++win_streak == STREAK_TO_WIN) {
                return true;
            }
        }
    }
    // Check horizontals.
    qDebug() << "Check horizontals";
    for (int x = 0; x < FIELD_SIZE; x++) {
        win_streak = 1;
        for (int y = 1; y < FIELD_SIZE; y++) {
            qDebug() << "x:" << x << " y:" << y;
            qDebug() << "[x,y]:" << this->cells[y][x]->text() << "[x,y-1]" << this->cells[y - 1][x]->text();
            if (this->cells[y][x]->text().isEmpty() || this->cells[y - 1][x]->text() != this->cells[y][x]->text()) {
                win_streak = 1;
            }
            else if (++win_streak == STREAK_TO_WIN) {
                return true;
            }
        }
    }
    for (int delta = 1; delta < FIELD_SIZE; delta++) {
        // Check left up diagonals.
        qDebug() << "Check left up diagonals";
        win_streak = 1;
        // Check lines [0,0],[1,1][2,2]/[0,1][1,2][2,3]... etc
        for (int x = 1, y = delta; y < FIELD_SIZE; x++, y++) {
            qDebug() << "x:" << x << " y:" << y;
            qDebug() << "[x,y]:" << this->cells[y][x]->text() << "[x-1,y-1]" << this->cells[y - 1][x - 1]->text();
            if (this->cells[y][x]->text().isEmpty() || this->cells[y][x]->text() != this->cells[y - 1][x - 1]->text()) {
                win_streak = 1;
            }
            else if (++win_streak == STREAK_TO_WIN) {
                return true;
            }
        }
        // Check left down diagonals.
        qDebug() << "Check left down diagonals";
        win_streak = 1;
        // Check lines [1,0][2,1][3,2]/[2,0],[3,1][4,2]... etc
        for (int y = 1, x = delta; x < FIELD_SIZE; x++, y++) {
            qDebug() << "x:" << x << " y:" << y;
            qDebug() << "[x,y]:" << this->cells[y][x]->text() << "[x-1,y-1]" << this->cells[y - 1][x - 1]->text();
            if (this->cells[y][x]->text().isEmpty() || this->cells[y][x]->text() != this->cells[y - 1][x - 1]->text()) {
                win_streak = 1;
            }
            else if (++win_streak == STREAK_TO_WIN) {
                return true;
            }
        }


        // Check right up diagonals.
        qDebug() << "Check right up diagonals";
        win_streak = 1;
        // Check lines [3,0],[2,1][1,2]/[2,1][1,0]... etc
        for (int x = delta, y = 0; x > 0 && y < FIELD_SIZE - 1; x--, y++) {
            qDebug() << "x:" << x << " y:" << y;
            qDebug() << "[x,y]:" << this->cells[y][x]->text() << "[x-1,y+1]" << this->cells[y + 1][x - 1]->text();
            if (this->cells[y][x]->text().isEmpty() || this->cells[y][x]->text() != this->cells[y + 1][x - 1]->text()) {
                win_streak = 1;
            }
            else if (++win_streak == STREAK_TO_WIN) {
                return true;
            }
        }
        // Check right down diagonals.
        qDebug() << "Check right down diagonals";
        win_streak = 1;
        // Check lines [0,3][1,2][2,1]/[0,2],[1,3][2,4]... etc
        for (int y = delta, x = FIELD_SIZE - 1; x > 0 && y < FIELD_SIZE - 1; x--, y++) {
            qDebug() << "x:" << x << " y:" << y;
            qDebug() << "[x,y]:" << this->cells[y][x]->text() << "[x-1,y+1]" << this->cells[y + 1][x - 1]->text();
            if (this->cells[y][x]->text().isEmpty() || this->cells[y][x]->text() != this->cells[y + 1][x - 1]->text()) {
                win_streak = 1;
            }
            else if (++win_streak == STREAK_TO_WIN) {
                return true;
            }
        }
    }
    // If we not found win streak.
    return false;
}

// @TODO Make better logic.
void MainWindow::computerMove() {
    int x = -1, y = -1;
    switch (this->current_step) {
    case 0:
        x = 1;
        y = 1;
        MakeComputerMove(x, y);
        return;
        break;

    case 1:
        if (cells[1][1]->text().isEmpty()) {
            x = 1;
            y = 1;
        }
        else {
            x = (getRandomNum(0, RANDOM_RANGE) < HALF_RANDOM_RANGE) ? 2 : 0;
            y = (getRandomNum(0, RANDOM_RANGE) < HALF_RANDOM_RANGE) ? 2 : 0;
        }
        MakeComputerMove(x, y);
        return;
        break;

    default:
        // Check for prefail.
        for (uint pre_y = 0; pre_y < FIELD_SIZE; pre_y++) {
            for (uint pre_x = 0; pre_x < FIELD_SIZE; pre_x++) {
                if (this->cells[pre_y][pre_x]->text().isEmpty()) {
                    const auto qs_figure = new QString(HUMAN_FIGURE);
                    this->cells[pre_y][pre_x]->setText(*qs_figure);
                    if (isWin()) {
                        x = pre_x;
                        y = pre_y;
                        MakeComputerMove(x, y);
                        return;
                    }
                    this->cells[pre_y][pre_x]->setText("");
                }
            }
        }
        // Check for prewin.
        for (uint pre_y = 0; pre_y < FIELD_SIZE; pre_y++) {
            for (uint pre_x = 0; pre_x < FIELD_SIZE; pre_x++) {
                if (this->cells[pre_y][pre_x]->text().isEmpty()) {
                    const auto qs_figure = new QString(COMPUTER_FIGURE);
                    this->cells[pre_y][pre_x]->setText(*qs_figure);
                    if (isWin()) {
                        x = pre_x;
                        y = pre_y;
                        MakeComputerMove(x, y);
                        return;
                    }
                    this->cells[pre_y][pre_x]->setText("");
                }
            }
        }
        while (x < 0 && y < 0) {
            // Select random cell.
            for (uint pre_y = 0; pre_y < FIELD_SIZE; pre_y++) {
                for (uint pre_x = 0; pre_x < FIELD_SIZE; pre_x++) {
                    if (this->cells[pre_y][pre_x]->text().isEmpty() && (getRandomNum(0, RANDOM_RANGE) < HALF_RANDOM_RANGE)) {
                        x = pre_x;
                        y = pre_y;
                        MakeComputerMove(x, y);
                        return;
                    }
                }
            }
        }
    }
}

void MainWindow::MakeComputerMove(uint x, uint y) {
    this->setSymbolToCell(this->cells[y][x], true);

    if (this->isWin()) {
        this->game_state = GAME_STATES::COMPUTER_WON;
        setStatusText();
    }
    else if(current_step == 9) {
        this->game_state = GAME_STATES::DRAW;
        setStatusText();
    }
}


void MainWindow::AddButtonsToCellsArray() {
    this->cells = new QPushButton **[FIELD_SIZE];
    for (uint y = 0; y < FIELD_SIZE; y++) {
        this->cells[y] = new QPushButton*[FIELD_SIZE];
    }
    this->cells[0][0] = this->ui->field00Btn;
    this->cells[0][1] = this->ui->field01Btn;
    this->cells[0][2] = this->ui->field02Btn;
    this->cells[1][0] = this->ui->field10Btn;
    this->cells[1][1] = this->ui->field11Btn;
    this->cells[1][2] = this->ui->field12Btn;
    this->cells[2][0] = this->ui->field20Btn;
    this->cells[2][1] = this->ui->field21Btn;
    this->cells[2][2] = this->ui->field22Btn;
}

void MainWindow::ConnectBtnSlots() {
    for (uint y = 0; y < FIELD_SIZE; y++) {
        for (uint x = 0; x < FIELD_SIZE; x++) {
            QObject::connect(this->cells[y][x], SIGNAL(clicked()), this, SLOT(onFieldBtn_clicked()));
        }
    }
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    AddButtonsToCellsArray();
    ConnectBtnSlots();
}

MainWindow::~MainWindow()
{
    for(uint i = 0; i < FIELD_SIZE; i++) {
        delete[] this->cells[i];
    }
    delete[] this->cells;
    delete ui;
}


void MainWindow::on_startBtn_clicked()
{
    this->ui->startBtn->setGeometry(120, 0, 111, 121);
    this->ui->startBtn->setText("Restart");
    for (uint y = 0; y < FIELD_SIZE; y++) {
        for (uint x = 0; x < FIELD_SIZE; x++) {
            this->cells[y][x]->setVisible(true);
            this->cells[y][x]->setText("");
            clearBgColor(x, y);
        }
    }
    bool first_player = (getRandomNum(0, RANDOM_RANGE) < HALF_RANDOM_RANGE);
    this->current_step = 0;
    this->game_state = GAME_STATES::IN_PROGRESS;
    if (first_player) {
        HUMAN_FIGURE = PLAYER_FIGURES::X;
        COMPUTER_FIGURE = PLAYER_FIGURES::O;
    }
    else {
        HUMAN_FIGURE = PLAYER_FIGURES::O;
        COMPUTER_FIGURE = PLAYER_FIGURES::X;
        this->computerMove();
    }
}

void MainWindow::clearBgColor(uint xCoord, uint yCoord) {
    this->cells[yCoord][xCoord]->setAutoFillBackground(false);
    this->cells[yCoord][xCoord]->update();
}

void MainWindow::setBgColor(uint xCoord, uint yCoord, Qt::GlobalColor color) {
    QPalette pal = this->cells[yCoord][xCoord]->palette();
    pal.setColor(QPalette::Button, QColor(color));
    this->cells[yCoord][xCoord]->setAutoFillBackground(true);
    this->cells[yCoord][xCoord]->setPalette(pal);
    this->cells[yCoord][xCoord]->update();
}

void MainWindow::setSymbolToCell(QPushButton* button, bool is_computer_step) {
    uint prev_x;
    uint prev_y;
    uint x;
    uint y;
    Qt::GlobalColor color;
    if (is_computer_step) {
        prev_x = this->computer_last_coord.xCoord;
        prev_y = this->computer_last_coord.yCoord;
        const auto qs_figure = new QString(COMPUTER_FIGURE);
        button->setText(*qs_figure);
        x = button->property(X_COORD_PROPERTY).toUInt();
        y = button->property(Y_COORD_PROPERTY).toUInt();
        this->computer_last_coord.xCoord = x;
        this->computer_last_coord.yCoord = y;
        color = COMPUTER_COLOR;
    }
    else {
        prev_x = this->human_last_coord.xCoord;
        prev_y = this->human_last_coord.yCoord;
        const auto qs_figure = new QString(HUMAN_FIGURE);
        button->setText(*qs_figure);
        x = button->property(X_COORD_PROPERTY).toUInt();
        y = button->property(Y_COORD_PROPERTY).toUInt();
        this->human_last_coord.xCoord = x;
        this->human_last_coord.yCoord = y;
        color = HUMAN_COLOR;
    }
    if (this->current_step > 1) {
        this->clearBgColor(prev_x, prev_y);
    }
    this->setBgColor(x, y, color);
    this->current_step++;
}


void MainWindow::onFieldBtn_clicked()
{
    if (this->game_state == GAME_STATES::IN_PROGRESS) {
        bool is_computer_step = (!(this->current_step % 2) && HUMAN_FIGURE == PLAYER_FIGURES::O) || (this->current_step % 2 && HUMAN_FIGURE == PLAYER_FIGURES::X);
        if (is_computer_step) {
            this->ui->textBrowser->setText("<p align=\"center\">You're too fast.</p><p align=\"center\">It's not your move now.</p>");
        }
        else {
            QPushButton* button = (QPushButton*)sender();
            setSymbolToCell(button, false);
            if (this->isWin()) {
                this->game_state = GAME_STATES::PLAYER_WON;
                setStatusText();
            }
            else if(current_step == 9) {
                this->game_state = GAME_STATES::DRAW;
                setStatusText();
            }
            else {
                this->computerMove();
            }
        }
    }
}

void MainWindow::setStatusText() {
    std::string text;
    uint delta;
    switch (this->game_state) {
    case GAME_STATES::DRAW:
        delta = getRandomNum(0, (DRAW_TEXTS_LENGTH - 1));
        text = DRAW_TEXTS[delta];
        break;


    case GAME_STATES::PLAYER_WON:
        delta = getRandomNum(0, (PLAYER_WINS_TEXTS_LENGTH - 1));
        text = PLAYER_WINS_TEXTS[delta];
        break;


    case GAME_STATES::COMPUTER_WON:
        delta = getRandomNum(0, (PLAYER_LOSE_TEXTS_LENGTH - 1));
        text = PLAYER_LOSE_TEXTS[delta];
        break;

    default:
        text = "<p align=\"center\">This game created by Aard.</p>";
    }
    this->ui->textBrowser->setText(QString::fromStdString(text));
}
